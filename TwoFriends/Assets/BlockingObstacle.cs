﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockingObstacle : MonoBehaviour
{
    [SerializeField] private GameObject blockUp;
    [SerializeField] private GameObject blockDown;


    public void DeactivateUselessObstacle(bool isUpObstacle)
    {
        blockUp.SetActive(isUpObstacle);
        blockDown.SetActive(!isUpObstacle);
    }
}
