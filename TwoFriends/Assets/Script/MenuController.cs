﻿using System.Collections;
using System.Numerics;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Vector2 = UnityEngine.Vector2;
#if UNITY_EDITOR
using UnityEditor;

#endif


public class MenuController : MonoBehaviour
{
    [SerializeField] private Text player1Text;
    [SerializeField] private Text player2Text;
    [SerializeField] private Text creditsText;

    private bool isLevelLoaded = false;
    private bool isPlayer1Ready = false;
    private bool isPlayer2Ready = false;


    private void Update()
    {
        HandleAnyInputForStart();
    }

    private void HandleAnyInputForStart()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            QuitGame();
        }

        isPlayer1Ready = Input.GetKey(KeyCode.RightControl) || Input.GetAxisRaw("Trigger RT") > 0.5f;
        isPlayer2Ready = Input.GetKey(KeyCode.CapsLock) || Input.GetAxisRaw("Trigger LT") > 0.5f;
     
        if (!isPlayer1Ready)
        {
            player1Text.text = "Press RT button";
        }
        else
        {
            player1Text.text = "Ready !";
        }
        
        
        if (!isPlayer2Ready)
        {
            player2Text.text = "Press LT button";
        }
        else
        {
            player2Text.text = "Ready !";
        }
        
        
        if(isPlayer1Ready && isPlayer2Ready && !isLevelLoaded)
        {
            StartLevel();
            isLevelLoaded = true;
        }
    }

    private void StartLevel()
    {
        StartCoroutine(LoadLevel("Level", "Start"));
    }

    IEnumerator LoadLevel(string sceneToLoad, string sceneToUnload)
    {
        AsyncOperation loadSceneAsync = SceneManager.LoadSceneAsync(sceneToLoad, LoadSceneMode.Additive);
        while (!loadSceneAsync.isDone)
        {
            yield return null;
        }

        SceneManager.UnloadSceneAsync(sceneToUnload);
    }


    private void QuitGame()
    {
#if UNITY_EDITOR
        EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }
}