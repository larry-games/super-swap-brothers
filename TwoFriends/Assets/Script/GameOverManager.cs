using UnityEngine;
using UnityEngine.UI;

public class GameOverManager : MonoBehaviour
{
    [SerializeField] private Text player1Score;
    [SerializeField] private Text player2Score;
    [SerializeField] private Text gameScore;
    
    private void OnEnable()
    {
        Player1 player1 = GameObject.FindWithTag("Player1").GetComponent<Player1>();
        player1Score.text = player1.hitCount +" hits\n" + player1.perfectJumpCount + " perfect jump !";

        Player2 player2 = GameObject.FindWithTag("Player2").GetComponent<Player2>();
        player2Score.text = player2.hitCount +" hits\n" + player2.perfectJumpCount + " perfect jump !";

        gameScore.text = "Score : " + LevelManager.currentScore;
    }
}