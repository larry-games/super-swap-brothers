﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Player2 : MonoBehaviour
{
    [SerializeField] Animator playerAnimator;
    [SerializeField] private Collider2D collider2d;
    [SerializeField] private float invincibilityTimeWhenHit;
    [SerializeField] private SpriteRenderer spriteRenderer;
    [SerializeField] private float jumpHeight;
    [SerializeField] private float jumpTime;
    [SerializeField] private AudioSource jumpAudioSource;
    [SerializeField] private AudioSource hitAudioSource;
    
    public delegate void OnHit();
    public static event OnHit RemoveOneLifePoint;
    
    private bool isJumping, isRotating, isHit, isGameLost, pause;
    private Sequence jumpingSequence;
    
    public bool OnTop;
    public int hitCount = 0;
    public int perfectJumpCount = 0;
    private bool isJumpLock = true;
    
    private void OnEnable()
    {
        LevelManager.OnPause += onPause;
        LevelManager.OnGameLost += stopMoving;
    }

    private void OnDisable()
    {
        LevelManager.OnPause -= onPause;
        LevelManager.OnGameLost -= stopMoving;
    }
    
    void Update()
    {
        if (!isGameLost && !pause && !isJumpLock)
        {
            if (Input.GetKey(KeyCode.Z) 
                || Input.GetKey(KeyCode.Q) 
                || Input.GetKey(KeyCode.S) 
                || Input.GetKey(KeyCode.D)
                ||Input.GetAxisRaw("Vertical")<-0.5f
                || Input.GetAxisRaw("Vertical")>0.5f
                || Input.GetAxisRaw("Horizontal")<-0.5f
                || Input.GetAxisRaw("Horizontal")>0.5f
                )
            {
                if (!isJumping && !isRotating)
                {
                    var sign = OnTop ? 1 : -1;
                    jumpingSequence = DOTween.Sequence();
                    jumpingSequence
                        .OnStart(() => isJumping = true)
                        .Append(transform.DOMove(Vector3.up * jumpHeight * sign, jumpTime / 2))
                        .Append(transform.DOMove(Vector3.up * 0.4f * sign, jumpTime / 2))
                        .OnComplete(() => isJumping = false);
                    jumpingSequence.Play();

                    jumpAudioSource.Play();
                }
            }
        }
    }

    void onPause(bool isPause)
    {
        pause = isPause;
        playerAnimator.enabled = !isPause;
        if (isJumping)
        {
            if (isPause)
                jumpingSequence.Pause();
            else
                jumpingSequence.Play();
        }
    }

    public void Hit()
    {
        if(!isGameLost && !isHit)
        {
            hitAudioSource.Play();
            RemoveOneLifePoint?.Invoke();
            StartCoroutine(deactivateInvincibility());
            hitCount++;
        }
    }

    IEnumerator deactivateInvincibility()
    {
        var normalColor = spriteRenderer.color;
        var transparentColor = normalColor;
        transparentColor.a = 0.5f;
        
        collider2d.enabled = false;
        isHit = true;
        var nbTimes = 5;
        while (nbTimes > 0)
        {
            spriteRenderer.color = transparentColor;
            yield return new WaitForSeconds(invincibilityTimeWhenHit/10);
            spriteRenderer.color = normalColor;
            yield return new WaitForSeconds(invincibilityTimeWhenHit/10);
            nbTimes--;
        }
        
        collider2d.enabled = true;
        isHit = false;
    }

    public void SetIsRotating(bool value)
    {
        isRotating = value;
    }

    public bool IsJumping() {
        return isJumping;
    }

    void stopMoving()
    {
        isGameLost = true;
        playerAnimator.enabled = false;
    }

    public void UnlockJump()
    {
        isJumpLock = false;
    }
}
