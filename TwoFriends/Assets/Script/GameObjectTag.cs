public enum GameObjectTag
{
    Player1,
    Player2,
    BlockingObstacle,
    NonBlockingObstacleForPlayer1,
    NonBlockingObstacleForPlayer2
}