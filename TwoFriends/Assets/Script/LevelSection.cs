﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public enum LevelSectionType
{
    EMPTY,
    ONLY_BLOCKING_OBSTACLES,
    ONLY_SWITCH_OBSTACLES,
    SYMMETRY,
    ASYMMETRY
}

public class LevelSection : MonoBehaviour
{
    [SerializeField] private GameObject blockingObstacle;
    [SerializeField] private GameObject goThroughObstacle;
    [SerializeField] private Vector2 minMaxPositionForObstacles;
    [SerializeField] private float yPositionForFloatingObstacle;
    [SerializeField] private Vector2 minMaxSpaceBetweenTwoObstacles;
    
    public delegate void SpawnNewSection(float xPos);
    public static event SpawnNewSection OnSpawnNewSection;
    
    private bool newSectionCreated;
    private bool stopgame, pause;
    
    float previousPosOfObstacle1 = 0;
    float previousPosOfObstacle2 = 0;

    private LevelManager levelManager;

    private void OnEnable()
    {
        LevelManager.OnPause += onPause;
        LevelManager.OnGameLost += stopMoving;
    }

    private void OnDisable()
    {
        LevelManager.OnPause -= onPause;
        LevelManager.OnGameLost -= stopMoving;
    }

    void FixedUpdate()
    {
        if (!stopgame && !pause)
        {
            var temp = transform.position;
            temp.x -= 0.5f * levelManager.GetSpeed();
            transform.position = temp;
            if (transform.position.x <= 30 && !newSectionCreated)
            {
                newSectionCreated = true;
                OnSpawnNewSection?.Invoke(transform.position.x - 30);
            }

            if (transform.position.x <= -55)
            {
                Destroy(gameObject);
            }
        }
    }

    void onPause(bool isPause)
    {
        pause = isPause;
    }

    public void InitializeLevelSectionWithObstacles(LevelSectionType levelSectionType, LevelManager lvlManager)
    {
        levelManager = lvlManager;
        if (previousPosOfObstacle1 == 0)
        {
            previousPosOfObstacle1 = minMaxPositionForObstacles.x;
            previousPosOfObstacle2 = minMaxPositionForObstacles.x;
        }

        /*if (levelSectionType == LevelSectionType.SYMMETRY)
        {
            while (previousPosOfObstacle1 + minMaxSpaceBetweenTwoObstacles.x <= minMaxPositionForObstacles.y)
            {
                var posForObstacle = previousPosOfObstacle1 +
                                     Random.Range(minMaxSpaceBetweenTwoObstacles.x, minMaxSpaceBetweenTwoObstacles.y);

                if (posForObstacle > minMaxPositionForObstacles.y)
                {
                    posForObstacle = minMaxPositionForObstacles.y;
                }

                var obstaclePrefab = getRandomObstacle();

                if (obstaclePrefab == blockingObstacle)
                {
                    var obstacleUp = Instantiate(obstaclePrefab, transform, true);
                    obstacleUp.transform.localPosition = new Vector3(posForObstacle, 0, 0);

                    var obstacleDown = Instantiate(obstaclePrefab, transform, true);
                    obstacleDown.transform.localPosition = new Vector3(posForObstacle, 0, 0);
                    obstacleDown.transform.DOLocalRotate(new Vector3(0, 0, 180), 0);
                }
                else
                {
                    var obstacle = Instantiate(obstaclePrefab, transform, true);
                    obstacle.transform.localPosition = new Vector3(posForObstacle, 0, 0);

                    if (Random.Range(0, 100) % 2 == 0)
                    {
                        obstacle.transform.DOLocalRotate(new Vector3(0, 0, 180), 0);
                    }
                }

                previousPosOfObstacle1 = posForObstacle;
            }
        }
        else*/ 
        if (levelSectionType == LevelSectionType.ASYMMETRY)
        {
            while (previousPosOfObstacle1 + minMaxSpaceBetweenTwoObstacles.x <= minMaxPositionForObstacles.y)
            {
                var obstaclePrefab = getRandomObstacle();
                if (obstaclePrefab == blockingObstacle)
                {
                    var posOfObstacle1 = previousPosOfObstacle1 +
                                         Random.Range(minMaxSpaceBetweenTwoObstacles.x, minMaxSpaceBetweenTwoObstacles.y);
                    var posOfObstacle2 = previousPosOfObstacle2 +
                                         Random.Range(minMaxSpaceBetweenTwoObstacles.x, minMaxSpaceBetweenTwoObstacles.y);

                    if (posOfObstacle1 > minMaxPositionForObstacles.y)
                    {
                        posOfObstacle1 = minMaxPositionForObstacles.y;
                    }
                
                    var obstacleUp = Instantiate(obstaclePrefab, transform);
                    obstacleUp.transform.localPosition = new Vector3(posOfObstacle1, 0, 0);
                    obstacleUp.GetComponent<BlockingObstacle>().DeactivateUselessObstacle(true);
                    previousPosOfObstacle1 = posOfObstacle1;

                    var obstacleDown = Instantiate(obstaclePrefab, transform);
                    obstacleDown.transform.localPosition = new Vector3(posOfObstacle2, 0, 0);
                    obstacleDown.GetComponent<BlockingObstacle>().DeactivateUselessObstacle(false);
                    previousPosOfObstacle2 = posOfObstacle2;
                }
                else
                {
                    var posForObstacle = previousPosOfObstacle1 +
                                         Random.Range(minMaxSpaceBetweenTwoObstacles.x, minMaxSpaceBetweenTwoObstacles.y);

                    if (posForObstacle > minMaxPositionForObstacles.y)
                    {
                        posForObstacle = minMaxPositionForObstacles.y;
                    }
                
                    var obstacle = Instantiate(obstaclePrefab, transform);
                    obstacle.transform.localPosition = new Vector3(posForObstacle, 0, 0);
                    if (Random.Range(0, 100) % 2 == 0)
                    {
                        obstacle.transform.DOLocalRotate(new Vector3(0, 180, 180), 0);
                    }
                    if (Random.Range(0, 100) % 2 == 0)
                    {
                        obstacle.GetComponent<GoThroughObstacle>().DeactivateARandomBlock();
                    }

                    previousPosOfObstacle1 = posForObstacle;
                    previousPosOfObstacle2 = posForObstacle;
                }
            }
        }
        else if(levelSectionType == LevelSectionType.ONLY_BLOCKING_OBSTACLES)
        {
            while (previousPosOfObstacle1 + minMaxSpaceBetweenTwoObstacles.x <= minMaxPositionForObstacles.y)
            {
                var posOfObstacle1 = previousPosOfObstacle1 +
                                     Random.Range(minMaxSpaceBetweenTwoObstacles.x, minMaxSpaceBetweenTwoObstacles.y);
                var posOfObstacle2 = previousPosOfObstacle2 +
                                     Random.Range(minMaxSpaceBetweenTwoObstacles.x, minMaxSpaceBetweenTwoObstacles.y);

                if (posOfObstacle1 > minMaxPositionForObstacles.y)
                {
                    posOfObstacle1 = minMaxPositionForObstacles.y;
                }

                var obstacleUp = Instantiate(blockingObstacle, transform);
                obstacleUp.transform.localPosition = new Vector3(posOfObstacle1, 0, 0);
                obstacleUp.GetComponent<BlockingObstacle>().DeactivateUselessObstacle(true);
                previousPosOfObstacle1 = posOfObstacle1;

                var obstacleDown = Instantiate(blockingObstacle, transform);
                obstacleDown.transform.localPosition = new Vector3(posOfObstacle2, 0, 0);
                obstacleDown.GetComponent<BlockingObstacle>().DeactivateUselessObstacle(false);
                previousPosOfObstacle2 = posOfObstacle2;
            }
        }
        else if (levelSectionType == LevelSectionType.ONLY_SWITCH_OBSTACLES)
        {
            while (previousPosOfObstacle1 + minMaxSpaceBetweenTwoObstacles.x <= minMaxPositionForObstacles.y)
            {
                var posForObstacle = previousPosOfObstacle1 +
                                     Random.Range(minMaxSpaceBetweenTwoObstacles.x, minMaxSpaceBetweenTwoObstacles.y);

                if (posForObstacle > minMaxPositionForObstacles.y)
                {
                    posForObstacle = minMaxPositionForObstacles.y;
                }
                
                var obstacle = Instantiate(goThroughObstacle, transform);
                obstacle.transform.localPosition = new Vector3(posForObstacle, 0, 0);
                if (Random.Range(0, 100) % 2 == 0)
                {
                    obstacle.transform.DOLocalRotate(new Vector3(0, 180, 180), 0);
                }
                if (Random.Range(0, 100) % 2 == 0)
                {
                    obstacle.GetComponent<GoThroughObstacle>().DeactivateARandomBlock();
                }

                previousPosOfObstacle1 = posForObstacle;
                previousPosOfObstacle2 = posForObstacle;
            }
        }
    }

    GameObject getRandomObstacle()
    {
        return Random.Range(0, 100) % 2 == 0 ? blockingObstacle : goThroughObstacle;
    }

    void stopMoving()
    {
        stopgame = true;
    }
}
