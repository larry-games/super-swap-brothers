﻿using UnityEngine;

public class NonBlockingObstacleScript : MonoBehaviour
{
    [SerializeField] private Animator doorAnimator;
    [SerializeField] private GameObjectTag tagToNotCollideWith;
    [SerializeField] private float pointsForThisObstacle;
    
    public delegate void ObstaclePassedThrough(float pointsWon);
    public static event ObstaclePassedThrough OnObstaclePassedThrough;
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (tagToNotCollideWith == GameObjectTag.Player1)
        {
            if (other.gameObject.CompareTag(GameObjectTag.Player1.ToString()))
            {
                OnObstaclePassedThrough?.Invoke(pointsForThisObstacle);
            }
            else if (other.gameObject.CompareTag(GameObjectTag.Player2.ToString()))
            {
                doorAnimator.SetTrigger("Glitch");
                other.GetComponent<Player2>().Hit();
            }
        }else if(tagToNotCollideWith == GameObjectTag.Player2)
        {
            if (other.gameObject.CompareTag(GameObjectTag.Player2.ToString()))
            {
                OnObstaclePassedThrough?.Invoke(pointsForThisObstacle);
            }
            else if (other.gameObject.CompareTag(GameObjectTag.Player1.ToString()))
            {
                doorAnimator.SetTrigger("Glitch");
                other.GetComponent<Player1>().Hit();
            }
        }
        
        
        
    }
}