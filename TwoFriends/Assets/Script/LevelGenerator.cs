﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LevelGenerator : MonoBehaviour
{
    [SerializeField] private LevelManager levelManager;
    [SerializeField] private GameObject levelSection;
    [SerializeField] private Transform movingLevelTransform;

    [SerializeField] private int sectionNumberToStartTutoJump;
    [SerializeField] private int sectionNumberToStopTutoJump;
    [SerializeField] private int sectionNumberToStartTutoSwitch;
    [SerializeField] private int sectionNumberToStopTutoSwitch;
    [SerializeField] private int sectionNumberToStartBoth;
    
    
    public delegate void DisplayTextToPlayers(string textInstuctions, string keyForPlayer1, string keyForPlayer2);
    public static event DisplayTextToPlayers OnDisplayTextToPlayers;

    private int currentSectionNumber=0;

    private LevelSectionType currentSectionType = LevelSectionType.EMPTY;
    
    void OnEnable ()
    {
        LevelSection.OnSpawnNewSection += instantiateNewLevelSection;
    }

    void OnDisable ()
    {
        LevelSection.OnSpawnNewSection -= instantiateNewLevelSection;
    }
    
    void Start()
    {
        var levelSectionGO = Instantiate(levelSection,movingLevelTransform);
        levelSectionGO.transform.localPosition = new Vector3(28, 0, 0);
        levelSectionGO.GetComponent<LevelSection>().InitializeLevelSectionWithObstacles(currentSectionType,levelManager);
        currentSectionNumber = 1;
        StartCoroutine(waitBeforeDisplayingMessages(
            0.5f, 
            "JUMP!",
            "Letters", 
            "Arrows")
        );
    }

    void instantiateNewLevelSection(float xPos)
    {
        var levelSectionGO = Instantiate(levelSection, movingLevelTransform);
        levelSectionGO.transform.localPosition =  new Vector3(111.92f + xPos, 0, 0);
        levelSectionGO.GetComponent<LevelSection>().InitializeLevelSectionWithObstacles(currentSectionType,levelManager);
        currentSectionNumber++;

        if (currentSectionNumber >= sectionNumberToStartTutoJump && currentSectionNumber<sectionNumberToStopTutoJump)
        {
            currentSectionType = LevelSectionType.ONLY_BLOCKING_OBSTACLES;
        }else if (currentSectionNumber >= sectionNumberToStopTutoJump && currentSectionNumber<sectionNumberToStartTutoSwitch)
        {
            currentSectionType = LevelSectionType.EMPTY;
            StartCoroutine(waitBeforeDisplayingMessages(
                5f, 
                "SWAP!",
                "RT", 
                "LT")
            );
        }else if (currentSectionNumber >= sectionNumberToStartTutoSwitch && currentSectionNumber<sectionNumberToStopTutoSwitch)
        {
            currentSectionType = LevelSectionType.ONLY_SWITCH_OBSTACLES;
        }else if (currentSectionNumber >= sectionNumberToStopTutoSwitch && currentSectionNumber<sectionNumberToStartBoth)
        {
            currentSectionType = LevelSectionType.EMPTY;
            StartCoroutine(waitBeforeDisplayingMessages(
                5f, 
                "BE READY!",
                "", 
                "")
            );
        }else
        {
            currentSectionType = LevelSectionType.ASYMMETRY;
        }
    }

    IEnumerator waitBeforeDisplayingMessages(float seconds, string textInstuctions, string textForPlayer1, string textForPlayer2)
    {
        yield return  new WaitForSeconds(seconds);
        OnDisplayTextToPlayers?.Invoke(textInstuctions, textForPlayer1,textForPlayer2);
    }
}
