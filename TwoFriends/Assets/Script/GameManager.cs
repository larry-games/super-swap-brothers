﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private float time = 0;

    private bool pause = false;

    private void Awake()
    {
        Application.targetFrameRate = 60;
        DontDestroyOnLoad(this);
    }

    private void Start()
    {
        SceneManager.LoadScene("Start", LoadSceneMode.Additive);
    }
}