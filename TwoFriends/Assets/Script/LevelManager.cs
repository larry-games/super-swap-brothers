﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Experimental.PlayerLoop;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public enum LevelEvent
{
    VERTICAL_FLIP,
    HORIZONTAL_FLIP,
    WAVE
}

public class LevelManager : MonoBehaviour
{
    [SerializeField] private Player1 player1;
    [SerializeField] private Player2 player2;
    [SerializeField] float timeIntervalForPlayerToSyncSwitch;
    [SerializeField] private int sharedLifePoints;
    [SerializeField] private float speed;
    [SerializeField] private Transform levelTransform;
    [SerializeField] private Transform playersTransform;
    [SerializeField] private AudioSource lightAudioSource;
    [SerializeField] private AudioSource darkAudioSource;
    [SerializeField] private AudioSource switchAudioSource;

    [Header("Events")]
    [SerializeField] private List<LevelEvent> levelEventsList;
    //[SerializeField] private LevelEvent eventToLaunchWithP;
    [SerializeField] private float timeForFirstRandomEvent;
    [SerializeField] private Vector2 minMaxFrequencyForEvents;
    [SerializeField] private Transform cameraContainer;
    [SerializeField] private Transform backgroundContainers;

    [Header("UI")] 
    [SerializeField] private GameObject pausePanel;
    [SerializeField] private Text instructions;
    [SerializeField] private Image player1Key;
    [SerializeField] private Image player2Key;
    [SerializeField] private Text timeLabel;
    [SerializeField] private Transform livesParent;
    [SerializeField] private GameObject lifePrefab;
    [SerializeField] private GameObject gameOverPanel;
    [SerializeField] private Text eventText;
    [SerializeField] private AudioSource eventAudioSource;
    [SerializeField] private AudioClip inversionClip;
    [SerializeField] private AudioClip waveClip;
    [SerializeField] private AudioClip gameOverClip;

    public delegate void GameLost();
    public static event GameLost OnGameLost;
    public delegate void Pause(bool isPause);
    public static event Pause OnPause;

    private bool isRotating;
    private bool player1WantsToSwitch, player2WantsToSwitch;
    private float time = 0;
    private bool isPaused = false;
    private int nextLifeToRemove;
    private bool isGameLost;
    private bool isEventHappening;

    public static float currentScore = 0;
    private float timeForNextEvent = 0;
    private float scoreMultiplier = 1, maxMultiplier = 8;

    private bool isSwapLock = true;
    private int messagesCount = 0;

    void OnEnable()
    {
        Player1.RemoveOneLifePoint += removeOneLifePoint;
        Player2.RemoveOneLifePoint += removeOneLifePoint;
        LevelGenerator.OnDisplayTextToPlayers += displayMessageToPlayers;
        ScoreCollider.OnObstaclePassed += addToScore;
        ScoreCollider.OnPerfectJump += perfectJump;
        NonBlockingObstacleScript.OnObstaclePassedThrough += addToScore;
    }

    void OnDisable()
    {
        Player1.RemoveOneLifePoint -= removeOneLifePoint;
        Player2.RemoveOneLifePoint -= removeOneLifePoint;
        LevelGenerator.OnDisplayTextToPlayers -= displayMessageToPlayers;
        ScoreCollider.OnObstaclePassed -= addToScore;
        NonBlockingObstacleScript.OnObstaclePassedThrough -= addToScore;
        currentScore = 0;
    }

    void Start()
    {
        timeForNextEvent = timeForFirstRandomEvent;
        initializeLives();
        player1.OnTop = true;
        player2.OnTop = false;
    }

    void Update()
    {
        if (!isGameLost)
        {
            if (!isPaused)
            {
                time += Time.deltaTime;
                //timeLabel.text = Mathf.Floor(time / 60).ToString("00") + ":" + Mathf.RoundToInt(time % 60).ToString("00");
                timeLabel.text = "Score : " + currentScore.ToString() + "\nx" + scoreMultiplier.ToString();

                if (time > 60) // A partir d'une minute on commence à accélerer la vitesse de scroll
                {
                    speed = Mathf.Lerp(speed, 3, Time.deltaTime / 120);
                }

                if ((time >= timeForNextEvent || Input.GetKey(KeyCode.P)) && !isEventHappening)
                {
                    timeForNextEvent = time + Random.Range(minMaxFrequencyForEvents.x, minMaxFrequencyForEvents.y);
                    var randomEvent = levelEventsList[Random.Range(0, levelEventsList.Count)];
                    StartCoroutine(launchEvent(0.5f, randomEvent, 0.5f));
                }

                if (!isSwapLock)
                {
                    if ((Input.GetKeyDown(KeyCode.RightControl) || Input.GetAxisRaw("Trigger LT") > 0.5f) && !isRotating &&
                        !player1WantsToSwitch && !player1.IsJumping())
                    {
                        //Debug.Log("player1WantsToSwitch");
                        player1WantsToSwitch = true;
                        StartCoroutine(player1DoenstWantsToSwitch());
                    }

                    if ((Input.GetKeyDown(KeyCode.CapsLock) || Input.GetAxisRaw("Trigger RT") > 0.5f) && !isRotating &&
                        !player2WantsToSwitch &&
                        !player2.IsJumping())
                    {
                        //Debug.Log("player2WantsToSwitch");
                        player2WantsToSwitch = true;
                        StartCoroutine(player2DoenstWantsToSwitch());
                    }

                    if (player1WantsToSwitch && player2WantsToSwitch)
                    {
                        //Debug.Log("players WantsToSwitch");
                        switchPlayers();
                    }
                }
            }

            if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.JoystickButton7)) // Joystick OPTION
            {
                handlePause();
            }
        }
        else
        {
            if (Input.GetKey(KeyCode.Space) || Input.GetKeyDown(KeyCode.Escape) || Input.GetKey(KeyCode.JoystickButton7))
            {
                restartGame();
            }
        }
    }

    IEnumerator launchEvent(float secondsBeforeLaunching, LevelEvent levelEvent, float secondsAfterLaunching)
    {
        isEventHappening = true;
        OnPause?.Invoke(true);

        if (levelEvent != LevelEvent.WAVE)
        {
            eventText.text = "ÇA TOURNE !";
            eventAudioSource.clip = inversionClip;
        }
        else
        {
            eventText.text = "ÇA BALANCE !";
            eventAudioSource.clip = waveClip;
        }
        

        var seq = DOTween.Sequence();
        seq.Insert(0, eventText.transform.DOScale(Vector3.one, 0.3f))
            .Insert(secondsBeforeLaunching + 0.4f, eventText.transform.DOScale(Vector3.zero, 0.3f));
        seq.Play();
        eventAudioSource.Play();
        
        yield return new WaitForSeconds(secondsBeforeLaunching);
        switch (levelEvent)
        {
            case LevelEvent.VERTICAL_FLIP:
                cameraContainer.DOLocalRotate(Vector3.up * 180, 0.4f, RotateMode.LocalAxisAdd);
                break;
            case LevelEvent.HORIZONTAL_FLIP:
                levelTransform.DOLocalRotate(Vector3.right * 180, 0.4f, RotateMode.LocalAxisAdd);
                backgroundContainers.DOLocalRotate(Vector3.right * 180, 0.4f, RotateMode.LocalAxisAdd).OnComplete(() =>
                {
                    lightAudioSource.mute = !lightAudioSource.mute;
                    darkAudioSource.mute = !darkAudioSource.mute;
                });
                break;
            case LevelEvent.WAVE:
                var waveSeq = DOTween.Sequence()
                    .Insert(0,cameraContainer.DOLocalRotate(Vector3.forward * 25, 3, RotateMode.LocalAxisAdd))
                    .Insert(10,cameraContainer.DOLocalRotate(Vector3.forward * -50, 6, RotateMode.LocalAxisAdd))
                    .Insert(20,cameraContainer.DOLocalRotate(Vector3.forward * 25, 3, RotateMode.LocalAxisAdd));
                waveSeq.Play();
                break;
            default: 
                break;
        }
        yield return new WaitForSeconds(secondsAfterLaunching);
        OnPause?.Invoke(false);
        isEventHappening = false;
    }

    void initializeLives()
    {
        nextLifeToRemove = sharedLifePoints - 1;
        for (int i = 0; i < sharedLifePoints; i++)
        {
            Instantiate(lifePrefab, livesParent);
        }
    }


    void handlePause()
    {
        isPaused = !isPaused;
        pausePanel.SetActive(isPaused);
        OnPause?.Invoke(isPaused);
    }

    void switchPlayers()
    {
        //Debug.Log("switchPlayers");
        player1WantsToSwitch = player2WantsToSwitch = false;
        player1.SetIsRotating(true);
        player2.SetIsRotating(true);
        isRotating = true;
        switchAudioSource.Play();
        playersTransform.DOLocalRotate(new Vector3(180, 0, 0), 0.2f, RotateMode.LocalAxisAdd).OnComplete(() =>
        {
            player1.SetIsRotating(false);
            player2.SetIsRotating(false);
            player1.OnTop = !player1.OnTop;
            player2.OnTop = !player2.OnTop;
        });
        StartCoroutine(delayReverseBy(0.35f));
    }

    IEnumerator player1DoenstWantsToSwitch()
    {
        yield return new WaitForSeconds(timeIntervalForPlayerToSyncSwitch);
        player1WantsToSwitch = false;
    }

    IEnumerator player2DoenstWantsToSwitch()
    {
        yield return new WaitForSeconds(timeIntervalForPlayerToSyncSwitch);
        player2WantsToSwitch = false;
    }

    IEnumerator delayReverseBy(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        isRotating = false;
    }

    void displayMessageToPlayers(string textInstructions, string keyForPlayer1, string keyForPlayer2)
    {
        messagesCount++;
        if (messagesCount == 1)
        {
            player1.UnlockJump();
            player2.UnlockJump();
        }else if (messagesCount == 2)
        {
            isSwapLock = false;
        }
        instructions.text = textInstructions;
        var fadeSeq = DOTween.Sequence()
            .Insert(0, instructions.transform.DOScale(Vector3.one, 0.5f))
            .Insert(5, instructions.transform.DOScale(Vector3.zero, 0.5f));
        
        if (!string.IsNullOrEmpty(keyForPlayer1))
        {
            player1Key.sprite = Resources.Load<Sprite>(keyForPlayer1);
            fadeSeq.Insert(0, player1Key.transform.DOScale(Vector3.one, 0.5f))
                    .Insert(5, player1Key.transform.DOScale(Vector3.zero, 0.5f));
        }

        if (!string.IsNullOrEmpty(keyForPlayer2))
        {
            player2Key.sprite = Resources.Load<Sprite>(keyForPlayer2);;
            fadeSeq.Insert(0, player2Key.transform.DOScale(Vector3.one, 0.5f))
                .Insert(5, player2Key.transform.DOScale(Vector3.zero, 0.5f));
        }
        fadeSeq.Play();
    }

    void addToScore(float points)
    {
        scoreMultiplier += 0.5f;
        if (scoreMultiplier > maxMultiplier)
            scoreMultiplier = maxMultiplier;
        currentScore += points * scoreMultiplier;
    }

    void perfectJump(string tag)
    {
        switch (tag)
        {
            case "Player1":
                player1.perfectJumpCount++;
                break;
            case "Player2":
                player2.perfectJumpCount++;
                break;
        }
    }

    void removeOneLifePoint()
    {
        scoreMultiplier = 1;
        if (!isGameLost)
        {
            sharedLifePoints--;

            if (nextLifeToRemove >= 0)
            {
                livesParent.GetChild(nextLifeToRemove).GetComponent<Image>().DOFade(0, 0.1f);
                nextLifeToRemove--;
            }

            if (sharedLifePoints <= 0)
            {
                gameOver();
            }
        }
    }

    void gameOver()
    {
        Debug.Log("GAME OVER");
        gameOverPanel.SetActive(true);
        isGameLost = true;
        OnGameLost?.Invoke();
        
        eventAudioSource.clip = gameOverClip;
        eventAudioSource.Play();
    }

    void restartGame()
    {
        SceneManager.LoadScene("Start");
    }

    public float GetSpeed()
    {
        return speed;
    }

    public float GetTime()
    {
        return time;
    }
}