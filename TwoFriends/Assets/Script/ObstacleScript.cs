﻿using UnityEngine;

public class ObstacleScript : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag(GameObjectTag.Player1.ToString()))
        {
            other.GetComponent<Player1>().Hit();
        }
        else if (other.gameObject.CompareTag(GameObjectTag.Player2.ToString()))
        {
            other.GetComponent<Player2>().Hit();
        }
    }
}