﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// This class is used to transition between scenes. This includes triggering all the things that need to happen on transition such as data persistence.
/// </summary>
public class SceneController : MonoBehaviour
{
    private Scene m_CurrentZoneScene;

    private bool m_Transitioning;

    private static SceneController instance;

    private static SceneController Instance
    {
        get
        {
            if (instance != null)
                return instance;

            instance = FindObjectOfType<SceneController>();

            if (instance != null)
                return instance;

            Create();

            return instance;
        }
    }

    public static bool Transitioning
    {
        get { return Instance.m_Transitioning; }
    }

    private static void Create()
    {
        GameObject sceneControllerGameObject = new GameObject("SceneController");
        instance = sceneControllerGameObject.AddComponent<SceneController>();
    }


    void Awake()
    {
        if (Instance != this)
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);

        m_CurrentZoneScene = SceneManager.GetActiveScene();
    }

    public static void RestartZone()
    {
        Instance.StartCoroutine(Instance.Transition(Instance.m_CurrentZoneScene.name, ScreenFader.FadeType.Loading));
    }

    public static void GameOver()
    {
        Instance.StartCoroutine(Instance.Transition(Instance.m_CurrentZoneScene.name, ScreenFader.FadeType.GameOver));
    }

    public static void TransitionToScene(TransitionPoint transitionPoint)
    {
        Instance.StartCoroutine(Instance.Transition(transitionPoint.newSceneName, ScreenFader.FadeType.Loading));
    }


    private IEnumerator Transition(string newSceneName, ScreenFader.FadeType type)
    {
        m_Transitioning = true;
        yield return StartCoroutine(ScreenFader.FadeSceneOut(type));
        yield return SceneManager.LoadSceneAsync(newSceneName);
        yield return StartCoroutine(ScreenFader.FadeSceneIn());

        m_Transitioning = false;
    }
}