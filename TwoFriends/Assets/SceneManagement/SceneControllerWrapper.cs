﻿using System;
using UnityEngine;

public class SceneControllerWrapper : MonoBehaviour
{
    public void RestartZone()
    {
        SceneController.RestartZone();
    }

    public void TransitionToScene(TransitionPoint transitionPoint)
    {
        SceneController.TransitionToScene(transitionPoint);
    }

    public void GameOver()
    {
        SceneController.GameOver();
    }
}