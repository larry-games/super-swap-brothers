﻿using UnityEditor;

[CustomEditor(typeof(TransitionPoint))]
public class TransitionStartEditor : Editor
{
    SerializedProperty m_TransitioningGameObjectProp;
    SerializedProperty m_TransitionTypeProp;
    SerializedProperty m_NewSceneNameProp;
    SerializedProperty m_DestinationTransformProp;
    SerializedProperty m_TransitionWhenProp;
    SerializedProperty m_ResetInputValuesOnTransitionProp;

    void OnEnable()
    {
        m_TransitioningGameObjectProp = serializedObject.FindProperty("transitioningGameObject");
        m_TransitionTypeProp = serializedObject.FindProperty("transitionType");
        m_NewSceneNameProp = serializedObject.FindProperty("newSceneName");
        m_DestinationTransformProp = serializedObject.FindProperty("destinationTransform");
        m_TransitionWhenProp = serializedObject.FindProperty("transitionWhen");
        m_ResetInputValuesOnTransitionProp = serializedObject.FindProperty("resetInputValuesOnTransition");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.PropertyField(m_TransitioningGameObjectProp);

        EditorGUILayout.PropertyField(m_TransitionTypeProp);
        EditorGUI.indentLevel++;
        EditorGUILayout.PropertyField((TransitionPoint.TransitionType) m_TransitionTypeProp.enumValueIndex ==
                                      TransitionPoint.TransitionType.SameScene
            ? m_DestinationTransformProp
            : m_NewSceneNameProp);

        EditorGUI.indentLevel--;

        EditorGUILayout.PropertyField(m_TransitionWhenProp);
        EditorGUILayout.PropertyField(m_ResetInputValuesOnTransitionProp);


        serializedObject.ApplyModifiedProperties();
    }
}