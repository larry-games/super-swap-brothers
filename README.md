# Super Swap Brothers

Super Swap Brothers is a horizontal runner game. 

It's a 2 player game played on a *single controller* ! 

Jump above the obstacles and cooperate with your partner to swap at the same time to go through your corresponding door ! But beware, unpredictable events can happen...


## Controls

### With an XBOX controller

Player 1 jump : A or B or X or Y
Player 1 swap : RT
Player 2 jump : any of the Dpad keys
Player 2 swap : LT

### With an AZERTY keyboard 
(but remember the game is made to be played with a controller)

|Action|Command|
|------|-------|
|Player 1 jump | any of the keyboard arrows |
|Player 1 swap | right ctrl |
|Player 2 jump | Z or Q or S or D |
|Player 2 swap | caps lock |


If you want a challenge, you can try to play solo and control both players !


## Game Screen

![Splashscreen](images/splashscreen.png)

![Gamescreen](images/gamescreen.png)